/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Fichero: Ejemplo0820ObjectInputStream.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
class Ejemplo0820Amigo implements java.io.Serializable {

  protected String nombre;
  protected double altura;

  public Ejemplo0820Amigo(String n, double a) {
    nombre = n;
    altura = a;
  }

  public void print() {
    System.out.println(nombre + " --> " + altura);
  }
}

public class Ejemplo0820ObjectInputStream {

  public static void main(String[] args)
          throws Exception {
    String fichero = "amigos.bin";
    String[] amigos = {"Pepe", "Juan"};
    double[] altura = {177.5, 170.25};
    // escribe los datos
    ObjectOutputStream fw = new ObjectOutputStream(
            new FileOutputStream(fichero));
    for (int i = 0; i < amigos.length; i++) {
      Ejemplo0820Amigo a = new Ejemplo0820Amigo(amigos[i], altura[i]);
      fw.writeObject(a);
    }
    fw.close();
    ObjectInputStream fr = new ObjectInputStream(
            new FileInputStream(fichero));
    //leer los datos del archivo
    try {
      Ejemplo0820Amigo a = null;
      a = (Ejemplo0820Amigo) fr.readObject();
      while (true) {
        a.print();
        a = (Ejemplo0820Amigo) fr.readObject();
      }
    } catch (EOFException e) {
    } finally {
      fr.close();
    }
  }
}
/* EJECUCION:
 Pepe --> 177.5
 Juan --> 170.25
 */
