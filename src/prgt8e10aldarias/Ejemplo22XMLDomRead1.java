/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.File;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Fichero: Ejemplo22XMLDomRead.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo22XMLDomRead1 {

  public static void main(String[] args) throws Exception {

    try {

      File fXmlFile = new File("Ejemplo21XMLDoom.xml");
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(fXmlFile);

      doc.getDocumentElement().normalize();

      System.out.println("Root element :" + 
              doc.getDocumentElement().getNodeName());

      NodeList nList = doc.getElementsByTagName("articulo");

      System.out.println("----------------------------");

      for (int temp = 0; temp < nList.getLength(); temp++) {

        Node nNode = nList.item(temp);

        System.out.println("\nCurrent Element :" + nNode.getNodeName());

        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

          Element eElement = (Element) nNode;

          System.out.println("Id : "
                  + eElement.getAttribute("id"));
          System.out.println("Nombre: "
                  + eElement.getElementsByTagName("nombre").item(0).getTextContent());
          System.out.println("Precio : "
                  + eElement.getElementsByTagName("precio").item(0).getTextContent());

        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
/* EJECUCION:
 1 Paco  12
 2 Juan  23
 * */
