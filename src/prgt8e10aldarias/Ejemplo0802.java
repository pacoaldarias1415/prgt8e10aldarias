/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e10aldarias;

import java.io.IOException;
import java.io.StringReader;

/**
 * Fichero: Ejemplo0802.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0802 {

  public static void main(String[] args) {
    String string = "Probando 1 2 3...";
    StringReader stringReader = new StringReader(string);
    int len = -1; // Numero de caracteres leidos
    char[] vector = new char[256]; // Caracteres lee de Reader
    try {
      while ((len = stringReader.read(vector, 0, 256)) != -1) {
        System.out.println("Reader leido: "
                + (new String(vector, 0, len)));
      }
    } catch (IOException ioe) {
    }
  }
}
/* EJECUCION:
Reader leido: Probando 1 2 3...
*/
